I added -Xmx200m, but with very slow start it works also with -Xmx180m for both 
max_routes.txt (100 000 routes and every route with 10 stations each, stations are random 0 - 100 000) 
and max_stations.txt (1000 routes with 1000 stations each, , stations are random 0 - 100 000).

Solution performance:

Map<Integer, Set<Route>> stationIdRoutes is the data structure 
that maps stationId to all it's routes. It's purpose is to in constant time 
return sets of routes that contain sourceStationId and sets of routes that contain destinationStationId. 
Next task is to iterate though one of the sets of routes and check if other contain the same route.
Complexity of that operation is linear (number of routes containing sourceStationId)
Next we need to find first route that has sourceStationId before destinationStationId 
which is about 50% chance
for every check (check searches for first occurrence of any source or destination stationId in route
which in linear and in worst case takes 999 iterations)
Route has it's id and since I return Route, it can also answer the question for finding any route
that matches. Route has LinkedHashSet which in constant time answers if stationId belongs to it
and allows to iterate until any of source or destination stationId is found, which answers the question if 
sourceStationId is equal to it which means we fond route.

Memory consumption for max_stations.txt data file:

1000 000 map entries is around 40MB
1000 routes for 1000 stations is around 52MB (LinkedHashSet uses more memory that HashSet)
1000 000 set entries is around 40MB
TOTAL around 132MB

I added RoutesControllerTest for performance test. It's ignored but when application is running and
annotation @Ignore is removed that test makes 50 threads, each making 50 calls to controller.
Total time is about 8,5s on my machine with no exceptions
