package com.goeuro.route.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class DirectConnectionResponseDto {

    @JsonProperty("dep_sid")
    private int sourceStationId;
    @JsonProperty("arr_sid")
    private int destinationStationId;
    @JsonProperty("direct_bus_route")
    private boolean directBusRoute;
}