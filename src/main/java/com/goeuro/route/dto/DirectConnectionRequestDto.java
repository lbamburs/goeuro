package com.goeuro.route.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class DirectConnectionRequestDto {

    private Integer sourceStationId;
    private Integer destinationStationId;
}
