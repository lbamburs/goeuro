package com.goeuro.route.service;

import com.goeuro.route.dto.DirectConnectionRequestDto;
import com.goeuro.route.model.Routes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoutesService {

    private Routes routes;

    @Autowired
    public RoutesService(Routes routes) {
        this.routes = routes;
    }

    public boolean isDirectBusRoute(DirectConnectionRequestDto directConnectionRequest) {
        return routes.findAnyRoute(directConnectionRequest.getSourceStationId(), directConnectionRequest.getDestinationStationId()).isPresent();
    }

}
