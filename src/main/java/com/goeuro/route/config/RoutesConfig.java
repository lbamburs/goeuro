package com.goeuro.route.config;

import com.goeuro.route.model.Routes;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RoutesConfig {

    @Bean
    public Routes routes(@Value("${routesDataPath}") String routesDataPath) {
        return new Routes(routesDataPath);
    }
}
