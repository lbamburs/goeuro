package com.goeuro.route.controller;

import com.goeuro.route.dto.DirectConnectionRequestDto;
import com.goeuro.route.dto.DirectConnectionResponseDto;
import com.goeuro.route.service.RoutesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class RoutesController {

    static final String HAS_DIRECT_CONNECTION_PATH =  "/api/direct";

    private RoutesService routesService;

    @Autowired
    public RoutesController(RoutesService routesService) {
        this.routesService = routesService;
    }

    @RequestMapping(value = HAS_DIRECT_CONNECTION_PATH, method = RequestMethod.GET)
    public DirectConnectionResponseDto hasDirectConnection(@RequestParam(value = "dep_sid") Integer sourceStationId,
                                                           @RequestParam(value = "arr_sid") Integer destinationStationId) {
        DirectConnectionRequestDto directConnectionRequest = createRequest(sourceStationId, destinationStationId);
        boolean directBusRoute = routesService.isDirectBusRoute(directConnectionRequest);
        return createResponse(directConnectionRequest, directBusRoute);
    }

    private DirectConnectionRequestDto createRequest(Integer sourceStationId, Integer destinationStationId) {
        return DirectConnectionRequestDto.builder()
                .sourceStationId(sourceStationId)
                .destinationStationId(destinationStationId)
                .build();
    }

    private DirectConnectionResponseDto createResponse(DirectConnectionRequestDto directConnectionRequest, boolean directConnectionExists) {
        return DirectConnectionResponseDto.builder()
                .sourceStationId(directConnectionRequest.getSourceStationId())
                .destinationStationId(directConnectionRequest.getDestinationStationId())
                .directBusRoute(directConnectionExists)
                .build();
    }
}
