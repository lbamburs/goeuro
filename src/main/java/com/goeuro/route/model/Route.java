package com.goeuro.route.model;

import com.goeuro.route.exception.ValidationException;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.LinkedHashSet;
import java.util.Set;

@ToString
@EqualsAndHashCode(of = "id")
public class Route {

    private int id;
    private Set<Integer> stationIds = new LinkedHashSet<>();

    public Route(int id) {
        this.id = id;
    }

    public void addStationId(Integer stationId) {
        stationIds.add(stationId);
    }

    public boolean hasDirectRoute(Integer sourceStationId, Integer destinationStationId) {
        validate(sourceStationId, destinationStationId);
        return hasStationId(sourceStationId) &&
               hasStationId(destinationStationId) &&
               isSourceBeforeDestination(sourceStationId, destinationStationId);
    }

    private void validate(Integer sourceStationId, Integer destinationStationId) {
        if (sourceStationId.equals(destinationStationId)) {
            throw new ValidationException("dep_sid must be different then arr_sid");
        }
    }

    private boolean hasStationId(Integer stationId) {
        return stationIds.contains(stationId);
    }

    private boolean isSourceBeforeDestination(Integer sourceStationId, Integer destinationStationId) {
        Integer firstStationId = stationIds.stream()
                .filter(stationId -> stationId.equals(sourceStationId) || stationId.equals(destinationStationId))
                .findFirst()
                .get();
        return firstStationId.equals(sourceStationId);
    }
}
