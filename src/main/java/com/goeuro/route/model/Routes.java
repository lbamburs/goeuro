package com.goeuro.route.model;

import lombok.SneakyThrows;
import lombok.ToString;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

@ToString
public class Routes {

    private Map<Integer, Set<Route>> stationIdRoutes = new HashMap<>();

    @SneakyThrows
    public Routes(String filePath) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            readRoutes(reader);
        }
    }

    public Optional<Route> findAnyRoute(Integer sourceStationId, Integer destinationStationId) {
        Set<Route> sourceRoutes = Optional.ofNullable(stationIdRoutes.get(sourceStationId)).orElse(Collections.emptySet());
        Set<Route> destinationRoutes = Optional.ofNullable(stationIdRoutes.get(destinationStationId)).orElse(Collections.emptySet());
        return sourceRoutes.stream()
                .filter(destinationRoutes::contains)
                .filter(route -> route.hasDirectRoute(sourceStationId, destinationStationId))
                .findFirst();
    }

    @SneakyThrows
    private void readRoutes(BufferedReader reader) {
        int routesNumber = Integer.parseInt(reader.readLine());
        for (int i = 0;i < routesNumber;i++) {
            readRoute(reader);
        }
    }

    @SneakyThrows
    private void readRoute(BufferedReader reader) {
        String[] routeLine = reader.readLine().split(" ");
        int routeId = Integer.parseInt(routeLine[0]);
        readRouteStations(routeLine, new Route(routeId));
    }

    private void readRouteStations(String[] routeLine, Route route) {
        for (int i = 1;i < routeLine.length;i++) {
            Integer stationId = Integer.valueOf(routeLine[i]);
            route.addStationId(stationId);
            stationIdRoutes.computeIfAbsent(stationId, k -> new HashSet<>()).add(route);
        }
    }

}
