package com.goeuro.core.config;

import com.goeuro.route.exception.ExceptionResponse;
import com.goeuro.route.exception.ValidationException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionAdvice extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return createBadRequest(ex.getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return createBadRequest(ex.getMessage());
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<Object> handleConstraintViolation(ValidationException ex, WebRequest request) {
        return createBadRequest(ex.getMessage());
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Object> handleConstraintViolation(RuntimeException ex, WebRequest request) {
        return createInternalServerError(ex.getMessage());
    }

    private ResponseEntity<Object> createBadRequest(String message) {
        return new ResponseEntity<>(ExceptionResponse.builder()
                .error(message)
                .build(), HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<Object> createInternalServerError(String message) {
        return new ResponseEntity<>(ExceptionResponse.builder()
                .error(message)
                .build(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
