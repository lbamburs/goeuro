package com.goeuro.route.model;

import static org.assertj.core.api.Assertions.*;

import com.goeuro.route.exception.ValidationException;
import com.goeuro.route.model.Route;
import com.google.common.collect.Lists;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

public class RouteTest {

    private static final String HAS_DIRECT_ROUTE_DATA_PROVIDER = "HAS_DIRECT_ROUTE_DATA_PROVIDER";

    @Test(dataProvider = HAS_DIRECT_ROUTE_DATA_PROVIDER)
    public void testHasDirectRoute(List<Integer> stations, int sourceStationId, int destinationStationId, boolean expectedHasDirectRoute) {
        // given
        Route route = new Route(1);
        stations.forEach(route::addStationId);

        // when
        boolean hasDirectRoute = route.hasDirectRoute(sourceStationId, destinationStationId);

        // then
        assertThat(hasDirectRoute).isEqualTo(expectedHasDirectRoute);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void shouldThrowExceptionForTheSameStationIds() {
        // given
        Route route = new Route(1);

        // when
        route.hasDirectRoute(1, 1);
    }

    @DataProvider(name = HAS_DIRECT_ROUTE_DATA_PROVIDER)
    private Object[][] hasDirectRouteDataProvider() {
        List<Integer> minimumStations = createMinimumStations();
        List<Integer> stations = createStations();

        return new Object[][] {
                {minimumStations, 1, 3, true},
                {minimumStations, 2, 3, true},
                {minimumStations, 3, 2, false},
                {stations, 5, 200, true},
                {stations, 5, 201, false},
        };
    }

    private List<Integer> createMinimumStations() {
        return Lists.newArrayList(1, 1, 2, 3);
    }

    private List<Integer> createStations() {
        return Lists.newArrayList(1, 5, 2, 8, 7, 200);
    }
}
