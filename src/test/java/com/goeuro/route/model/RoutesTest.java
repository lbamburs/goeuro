package com.goeuro.route.model;

import static org.assertj.core.api.Assertions.*;

import com.goeuro.route.model.Route;
import com.goeuro.route.model.Routes;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Optional;

public class RoutesTest {

    private static final String FIND_ANY_ROUTE_DATA_PROVIDER = "FIND_ANY_ROUTE_DATA_PROVIDER";

    @Test(dataProvider = FIND_ANY_ROUTE_DATA_PROVIDER)
    public void testFindAnyRoute(int sourceStationId, int destinationStationId, boolean expectedRoute) {
        // given
        Routes routes = new Routes(getClass().getClassLoader().getResource("data/example.txt").getFile());

        // when
        Optional<Route> route = routes.findAnyRoute(sourceStationId, destinationStationId);

        // then
        assertThat(route.isPresent()).isEqualTo(expectedRoute);
    }

    @DataProvider(name = FIND_ANY_ROUTE_DATA_PROVIDER)
    public Object[][] findAnyRouteDataProvider() {
        return new Object[][] {
                {0, 1, true},
                {0, 2, true},
                {0, 4, true},
                {6, 5, true},
                {6, 4, true},
                {0, 5, false},
                {10, 20, false}
        };
    }
}
