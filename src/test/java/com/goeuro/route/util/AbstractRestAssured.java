package com.goeuro.route.util;

import com.jayway.restassured.RestAssured;
import org.testng.annotations.BeforeClass;

public abstract class AbstractRestAssured {

    @BeforeClass
    public static void beforeClass() {
        setPort();
    }

    private static void setPort() {
        RestAssured.port = 8088;
    }
}
