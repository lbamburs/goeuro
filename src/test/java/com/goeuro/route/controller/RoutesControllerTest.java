package com.goeuro.route.controller;

import static com.jayway.restassured.RestAssured.*;
import static org.assertj.core.api.Assertions.*;
import com.goeuro.route.util.AbstractRestAssured;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

@Slf4j
public class RoutesControllerTest extends AbstractRestAssured {

    @Test(invocationCount = 50, threadPoolSize = 50)
    @Ignore
    public void testPerformance() {
        for (int i = 1;i <= 50;i++) {
            log.debug("RoutesControllerTest check {}", i);
            int statusCode = get(RoutesController.HAS_DIRECT_CONNECTION_PATH + "?dep_sid=31934&arr_sid=74013").getStatusCode();
            assertThat(statusCode).isEqualTo(HttpStatus.OK.value());
        }
    }
}
